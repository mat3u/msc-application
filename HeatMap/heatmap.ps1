Param(
	[string]$data = "data.csv",
    [string]$screenShot = "screen.png"
)

Write-Host -Fore Cyan "Mateusz Stasch (c) 2013 | matt.stasch@gmail.com"

if(!(Test-Path $data) -or $data -eq "") {
    Write-Host -Fore Red "File '$data' does not exists!"
    return
}

if(!(Test-Path $screenShot) -or $screenShot -eq "") {
    Write-Host -Fore Red "File '$screenShot' does not exists!"
    return
}

if(Test-Path 'C:\Program Files (x86)\ImageMagick*\') {

$width = 1920
$height = 1200

set-alias identify 'C:\Program Files (x86)\ImageMagick*\identify.exe'
set-alias composite 'C:\Program Files (x86)\ImageMagick*\composite.exe'
set-alias conv 'C:\Program Files (x86)\ImageMagick*\convert.exe'
set-alias hm .\HeatMap.Cmd.exe

Write-Progress -activity "Heatmap rendering" -status "Preparation..." -percentComplete 0
identify .\screen.png | %{ $t = $_.split(' ')[2].split('x'); $width = $t[0]; $height = $t[1]; }

Write-Progress -activity "Heatmap rendering ($width x $height)" -status "Grayscale heatmap generation..." -percentComplete 5
hm $data $width $height

Write-Progress -activity "Heatmap rendering ($width x $height)" -status "Image processing..." -percentComplete 20
#conv org.png -negate full.png

Write-Progress -activity "Heatmap rendering ($width x $height)" -status "Colorizing..." -percentComplete 50
conv org.png colors.png -fx "v.p{0,u*v.h}" final.png

Write-Progress -activity "Heatmap rendering ($width x $height)" -status "Rendering heatmap on screenshot..." -percentComplete 90
composite -blend 40% .\final.png $screenShot heatmap.png

Write-Progress -activity "Heatmap rendering ($width x $height)" -status "Clean up..." -percentComplete 99

rm org.png
rm full.png
rm final.png

} else {
    Write-Host -Fore Red "ImageMagick not found!`nYou can download it from: http://http://www.imagemagick.org"
}